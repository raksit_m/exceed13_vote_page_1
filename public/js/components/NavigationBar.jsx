var React = require('react');
var ReactBootstrap = require('react-bootstrap');

var NavigationBar = React.createClass({
  render: function() {
    var NavBar = ReactBootstrap.Navbar;
    var NavbarBrand = ReactBootstrap.NavbarBrand;
    var Nav    = ReactBootstrap.Nav;
    var NavItem = ReactBootstrap.NavItem;

    var Image = ReactBootstrap.Image;

    return (
      <NavBar fixedTop={true}>
        <Nav pullRight>
          <NavItem className="navbar-profile page-scroll" eventKey={1}>Sign in</NavItem>
        </Nav>
      </NavBar>
    );
  }
});

module.exports = NavigationBar;