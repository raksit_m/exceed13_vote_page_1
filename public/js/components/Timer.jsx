var React = require('react');

var TimerActions = require('../actions/TimerActions');

var DEFAULT_TIMEOUT = 60;


function toHHMMSS(number) {
	var time = parseInt(number, 10);

	var times = [];

	var hours = Math.floor(time / (60 * 60));
	var minutes = Math.floor((time - (hours * 60 * 60)) / 60);
	var seconds = time - (hours * 60 * 60) - (minutes * 60);

	if(hours < 10) {hours = "0" + hours};
	if(minutes < 10) {minutes = "0" + minutes};
	if(seconds < 10) {seconds = "0" + seconds};

	times[0] = hours;
	times[1] = minutes;
	times[2] = seconds;

	return times;
}	

var Timer = React.createClass({

	getInitialState() {
		this.defaultTimeout = this.props.defaultTimeout || DEFAULT_TIMEOUT;

		return {
			timeLeft: this.defaultTimeout
		};
	},

	componentDidMount: function() {
    	this._tick();
  	},

	componentWillUnmount() {
		clearTimeout(this.interval);
	},

	componentDidUpdate() {
		if(this.props.status.isStart() && this.interval === undefined) {
			this._tick();
		}

		// Reset Logic...
		else if(this.props.status.isReset()) {
			this.setState({
				timeLeft: this.defaultTimeout
			});
		}
	},

	handlestopTimer() {
		TimerActions.stopTimer();
	},

	_tick() {
		var self = this;
		this.interval = setTimeout(function() {
			if(self.props.status.isStop()) {
				self.interval = undefined;
				return;
			}

			self.setState({
				timeLeft: self.state.timeLeft - 1
			});

			if(self.state.timeLeft <= 0) {
				self.handlestopTimer();
			}

			self._tick(); 

		}, 1000);
	},

	render() {
		var times = toHHMMSS(this.state.timeLeft)
		return (
			<div className="time">
				<div style={{borderRight: "1px solid #ffffff"}} className="box-time">
					<h1>{times[0]}</h1>
					<p>Hours</p>
				</div>

				<div style={{borderRight: "1px solid #ffffff"}} className="box-time">
					<h1>{times[1]}</h1>
					<p>Minutes</p>
				</div>

			<div className="box-time">
				<h1>{times[2]}</h1>
				<p>Seconds</p>
			</div>
			</div>
		);
	}

});

module.exports = Timer;