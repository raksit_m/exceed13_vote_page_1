var React = require('react');

var ReactBootstrap = require('react-bootstrap');

var CircularProgressBar = require('../circular-progressbar/wrapper.jsx');

var Progress = React.createClass({
	render() {
		return (
			<div>
				<CircularProgressBar percent=25/>
			</div>
		);
	}
});