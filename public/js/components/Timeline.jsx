var React = require('react');

var ReactBootstrap = require('react-bootstrap');

var CircularProgressBar = require('./circular-progressbar/wrapper.jsx');

var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;


var Timeline = React.createClass({
	render() {
		return (
			<div>
				<Grid>
					<Row className="timeline-element">
						<Col md={3} mdOffset={2}>
							<div className="timeline-panel">
							<h4>Sign in</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl metus.</p>
							<span className="btn-vote">Sign in</span>
							</div>
						</Col>
						<Col md={2}>
							<CircularProgressBar percent={100} />
						</Col>
					</Row>

					<Row className="timeline-element">
						<Col md={2} mdOffset={5}>
							<CircularProgressBar percent={75} />
						</Col>
						<Col md={3}>
							<div className="timeline-panel-inverted">
							<h4>Complete Project Document</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl metus.</p>
							<span className="btn-vote">Complete</span>
							</div>
						</Col>
					</Row>

					<Row className="timeline-element">
						<Col md={3} mdOffset={2}>
							<div className="timeline-panel">
							<h4>Vote Project</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl metus.</p>
							<span className="btn-vote">Vote</span>
							</div>
						</Col>
						<Col md={2}>
							<CircularProgressBar percent={50} />
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
});

module.exports = Timeline;