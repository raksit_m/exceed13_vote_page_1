var React = require('react');

var ReactBootstrap = require('react-bootstrap');

var Waypoint = require('react-waypoint');

var PropTypes = React.PropTypes;

var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;

var Awards = require('./Awards.jsx');
var Timeline = require('./Timeline.jsx');

var Content = React.createClass({

	getInitialState() {
		return {
			isIntroEnter: false,
			isTimelineEnter: false
		};
	},

	_getContentIntroClassName() {
		return "content-intro animated" + this._getContentIntroFilter();
	},

	_getContentTimelineClassName() {
		return "content-timeline animated" + this._getContentTimelineFilter();
	},

	_enterIntro() {
		this.setState({
			isIntroEnter: true,
			isTimelineEnter: this.state.isTimelineEnter
		});
	},

	_enterTimeline() {
		this.setState({
			isIntroEnter: this.state.isIntroEnter,
			isTimelineEnter: true
		});
	},

	_getContentIntroFilter() {
		if(this.state.isIntroEnter) {
			return " fadeInUp";
		}

		else return "";
	},

	_getContentTimelineFilter() {
		if(this.state.isTimelineEnter) {
			return " fadeInLeft";
		}

		else return "";
	},

	render() {
		return (
			<div>
				<div className="content">
					<section className={this._getContentIntroClassName()}>

					<Waypoint
					onEnter={this._enterIntro.bind(this)}
				 	/>

					<h2>Awards</h2>
					<p style={{marginBottom: "75px"}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl metus.</p>
					<Awards />
					</section>

					<section className={this._getContentTimelineClassName()}>

					<Waypoint
					onEnter={this._enterTimeline.bind(this)}
				 	/>
				 	
						<h2>Timeline</h2>
						<p style={{textAlign: "center", marginBottom: "75px"}}>Lorem ipsum dolor sit amet, consectetur</p>
							<Grid>
							    <Row>
							      <Col lg={12}>
							      	<Timeline />
							      </Col>						   
							    </Row>
						    </Grid>
					</section>
				</div>
			</div>
		);
	}
});

module.exports = Content;