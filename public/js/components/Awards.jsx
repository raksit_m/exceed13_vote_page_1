var React = require('react');

var ReactBootstrap = require('react-bootstrap');

var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;

var Image = ReactBootstrap.Image;


var Awards = React.createClass({
	render() {
		return (
			<div>
				<Grid>
					<Row>
						<Col className="award-element" sm={6} md={3}>
							<Image src="../public/img/best_of_hardware.svg" responsive></Image>
							<h4>Best of Hardware</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl metus.</p>
						</Col>

						<Col className="award-element" sm={6} md={3}>
							<Image src="../public/img/best_of_software.svg" responsive></Image>
							<h4>Best of Software</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl metus.</p>
						</Col>

						<Col className="award-element" sm={6} md={3}>
							<Image src="../public/img/popular_vote.svg" responsive></Image>
							<h4>Popular Vote</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl metus.</p>
						</Col>

						<Col className="award-element" sm={6} md={3}>
							<Image src="../public/img/top_rated.svg" responsive></Image>
							<h4>Top Rated</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl metus.</p>
						</Col>
					</Row>
				</Grid>
			</div>
		);
	}
});

module.exports = Awards;