var React = require('react');
var ReactBootstrap = require('react-bootstrap');

var TimerStore = require('../stores/TimerStore');

var Timer = require('./Timer.jsx');
var NavigationBar = require('./NavigationBar.jsx');
var Content = require('./Content.jsx');

var Image = ReactBootstrap.Image;

function getTimerStatus() {
	return {
		timerStatus: TimerStore.getTimerStatus()
	};
}

var App = React.createClass({

	getInitialState() {
		return getTimerStatus();
	},

	componentDidMount() {
		TimerStore.addChangeListener(this._onChange);
	},

	componentWillUnmount() {
		TimerStore.removeChangeListener(this._onChange);
	},

	_onChange() {
		this.setState(getTimerStatus());
	},

	render() {
		return (
			<div>
				<div id="page-top">
					<NavigationBar />
					<Image className="img-logo-big" src="../public/img/logo_inverse.png" responsive />
						<p style={{fontSize: "1.5em" , textAlign: "center"}}>eXceed vote will be closed in</p>
						<Timer status={this.state.timerStatus} defaultTimeout='86400' onTimeout={this.handlestopTimer} />
				</div>

				<Content />
			</div>
		);
	}
});

module.exports = App;