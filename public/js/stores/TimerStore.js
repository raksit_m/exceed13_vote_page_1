var AppDispatcher = require('../dispatchers/AppDispatcher');
var TimerConstants = require('../constants/TimerConstants');

var EventEmitter = require('events').EventEmitter;

var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var TimerStatus = function(status) {
	this.status = status;
};

TimerStatus.prototype.isStart = function() {
	return this.status === 'start';
};

TimerStatus.prototype.isStop = function() {
	return this.status === 'stop';
};

TimerStatus.prototype.isReset = function() {
	return this.status === 'reset';
};

TimerStatus.prototype.reset = function() {
	if(this.isReset()) {
		this.status = 'start';
	}
};

var _timerStatus = new TimerStatus('start');

function _startTimer() {
	_timerStatus = new TimerStatus('start');
}

function _stopTimer() {
	_timerStatus = new TimerStatus('stop');
}

function _resetTimer() {
	_timerStatus = new TimerStatus('reset');
}

var TimerStore = assign({}, EventEmitter.prototype, {
	getTimerStatus: function() {
		return _timerStatus;
	},

	emitChange: function() {
		this.emit(CHANGE_EVENT);
	},

	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	}
});

AppDispatcher.register(function(action) {

	switch(action.actionType) {
		case TimerConstants.START_TIMER:
			_startTimer();
			break;
		case TimerConstants.STOP_TIMER:
			_stopTimer();
			break;
		case TimerConstants.RESET_TIMER:
			_resetTimer();
			break;
		default:
			// do nothing
	}

	// When program finishes callback, then notify main components.
	TimerStore.emitChange();
});

module.exports = TimerStore;

