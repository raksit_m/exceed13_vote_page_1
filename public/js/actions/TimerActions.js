var AppDispatcher = require('../dispatchers/AppDispatcher');
var TimerConstants = require('../constants/TimerConstants');

var TimerActions = {

	startTimer: function() {
		AppDispatcher.dispatch({
			actionType: TimerConstants.START_TIMER
		});
	},

	stopTimer: function() {
		AppDispatcher.dispatch({
			actionType: TimerConstants.STOP_TIMER
		});
	},

	resetTimer: function() {
		AppDispatcher.dispatch({
			actionType: TimerConstants.RESET_TIMER
		});
	}
};

module.exports = TimerActions;