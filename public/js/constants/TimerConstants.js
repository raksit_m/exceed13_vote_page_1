var keyMirror = require('keymirror');

module.exports = keyMirror({
	START_TIMER: null,
	STOP_TIMER: null,
	RESET_TIMER: null
	// more actions here...
});