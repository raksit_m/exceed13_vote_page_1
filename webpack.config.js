var config = {
   entry: './public/js/main.js',
	
   output: {
      path:'./',
      filename: './public/js/index.js',
   },
	
   devServer: {
      inline: true,
      port: 7878
   },
	
   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',
				
            query: {
               presets: ['es2015', 'react']
            }
         },

         { test: /\.css$/, loader: "style-loader!css-loader?importLoaders=1" },
         { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' }
      ]
   }
}

module.exports = config;